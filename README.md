# Steplix Config

Steplix Config is a Node.js configuration helper.

## Index

* [Download & Install][install].
* [How is it used?][how_is_it_used].
* [Tests][tests].

## Download & Install

### NPM
```bash
    npm install steplix-config
```

### Source code
```bash
$ git clone https://gitlab.com/steplix/SteplixConfig.git
$ cd SteplixConfig
$ npm install
```

## How is it used?

 + Static.
 + Extensible.
 + Configurable by environment.
 + Try a function to automatically require all files and folders recursively, keeping the naming structure.

### Configure

#### Environment options

| Environment variable | Value         | Default value | Description                                 |
|:---------------------|:--------------|:--------------|:--------------------------------------------|
| CONFIG_SEPARATOR     | string        | "."           | Indicate config key separator.              |
| CONFIG_PRIVATE       | true|false    | false         | Indicate if config object has raw property. |

#### Folder structure
```
 <My config folder name>
 |
 | -> index.js
 |    // Content: `module.exports = require('steplix-config').require(module, __dirname);`
 |
 | -> server.js
 |    // Example server file: `module.exports = { port: 8080 };`
 | -> my-config-file.js
 |    // Other example config file: `module.exports = { key: value, a: 'b', c: 1 };`
 | -> ...
 |
 | -> my-config-folder
 |     | -> types.js
 |          // Other example config file anidate: `module.exports = { 1: 'immediate', 2: 'periodic', 3: 'scheduled' };`
 |     | -> ...
 |
 | -> production
 |     | -> types.js
 |          // Override types for production environment.
 |     | -> ...
```

```js
const config = require('steplix-config');

// Simple usage
config('server.port'); // return: port value
config('my.not.exist.config'); // return: undefined
config('my.not.exist.config', 'my-default-value'); // return: 'my-default-value'

// Extend with object
config.extend({
  my: {
    config: {
      object: {
        hello: 'world!'
      }
    }
  }
});
config('my.config.object.hello'); // return: 'world!'
config('my.config.object'); // return: { hello: 'world!' }
config('my.config'); // return: { object: { hello: 'world!' } }

// Require all files on current folder
config.require(module, __dirname);
// Require all files on other folder
config.require(module, './my/other/folder');
// See more options on https://www.npmjs.com/package/require-directory#options
config.require(module, __dirname, {
  // Blacklist
  exclude: /dontinclude\.js$/,
  // Rename keys
  rename: name => name.toUpperCase()
});
```

## Tests

In order to see more concrete examples, **I INVITE YOU TO LOOK AT THE TESTS :)**

### Run the unit tests
```sh
npm test
```

<!-- deep links -->
[install]: #download--install
[how_is_it_used]: #how-is-it-used
[tests]: #tests
