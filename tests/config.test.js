'use strict';

const config = require('./config');

describe('Config', () => {
    describe('get', () => {
        it('should return correct value for "server.port"', done => {
            const result = config('server.port');

            expect(result).to.be.a('number').equal(8080);
            done();
        });

        it('should return correct value for "server.host"', done => {
            const result = config('server.host');

            expect(result).to.be.a('string').equal('0.0.0.0');
            done();
        });

        it('should return correct value for "my.other.anidate.config"', done => {
            const result = config('my.other.anidate.config');

            expect(result).to.be.a('string').equal('Hello testing World!');
            done();
        });
    });
});
